import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./components/authentication/authentication.module').then(module => module.AuthenticationModule)
  },
  {
    path: 'home',
    canActivate: [false],
    children: [
      {
        path: '',
        loadChildren: () => import('./components/home/home.module').then(module => module.HomeModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
